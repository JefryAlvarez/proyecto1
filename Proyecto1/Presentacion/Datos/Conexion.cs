﻿
using Npgsql;
using Presentacion.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto1
{
    class Conexion
    {
        public NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public NpgsqlConnection ConexionBD()
        {
            string servidor = "localhost";
            int puerto = 5432;
            string usuario = "postgres";
            string clave = "Jefryalv07";
            string baseDatos = "Progra3";

            string cadenaConexion = "Server=" + servidor + ";" + "Port=" + puerto + ";" + "User Id=" + usuario + ";" + "Password=" + clave + ";" + "Database=" + baseDatos;
            conexion = new NpgsqlConnection(cadenaConexion);
            conexion.Open();

            return conexion;
        }

        internal void insertarA(Aerolinea a)
        {
            try
            {
                ConexionBD();
                cmd = new NpgsqlCommand("INSERT INTO proyecto1.aerolinea(nombre, ano_fundacion, tipo)VALUES('"+a.nombre+"', '" + a.ano_fundacion + "', '" + a.tipo + "')", conexion);

                cmd.ExecuteNonQuery();
                conexion.Close();
            }catch(Exception e)
            {
                if (e.Message.Contains("unq_nombre"))
                {
                    MessageBox.Show("La aerolinea anteriormente registrada");
                }
            }
            
        }

        public void InsertarUsuarios(Usuario us)
        {
           
            try
            {
                ConexionBD();
                /*conexion.Open();*/
                cmd = new NpgsqlCommand("INSERT INTO proyecto1.usuarios(cedula, nombre, edad, contrasena, tipo)VALUES ('" + us.cedula + "', '" + us.nombre + "','" + us.edad + "', '" + us.contrasena + "','Pasajero')", conexion);

                cmd.ExecuteNonQuery();
                conexion.Close();
            }
            catch(Exception e)
            {
                if (e.Message.Contains("unq_ced")){
                    MessageBox.Show("Ya existe un usuario con esta cedula.");
                    
                }
            }
        }
        public List<Usuario> ConsultarDatos()
        {

            
            ConexionBD();
            List<Usuario> lista = new List<Usuario>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT  cedula, nombre, edad, contrasena, tipo FROM proyecto1.usuarios", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarusuario(dr));
                }
            }
            conexion.Close();
            return lista;
        }
        public Usuario login(Usuario us)
        {
            ConexionBD();
            Usuario usuario = new Usuario();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT  cedula, nombre, edad, contrasena, tipo FROM proyecto1.usuarios where cedula= "+us.cedula, conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    usuario= cargarusuario(dr);
                }
            }
            conexion.Close();
            return usuario;
        }
         
        
        public Usuario cargarusuario(NpgsqlDataReader dr)
        {
            Usuario us = new Usuario();
            us.cedula = dr.GetInt32(0);
            us.nombre = dr.GetString(1);
            us.edad = dr.GetInt32(2);
            us.contrasena = dr.GetString(3);
            us.tipo = dr.GetString(4);
            return us;

        }
        public List<Aerolinea> ConsultarDatosAerolinea()
        {


            ConexionBD();
            List<Aerolinea> lista = new List<Aerolinea>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT  id, nombre, ano_fundacion, tipo FROM proyecto1.aerolinea", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarAerolinea(dr));
                }
            }
            conexion.Close();
            return lista;
        }
        public Aerolinea cargarAerolinea(NpgsqlDataReader dr)
        {
            Aerolinea us = new Aerolinea();
            us.id = dr.GetInt32(0);
            us.nombre = dr.GetString(1);
            us.ano_fundacion = dr.GetInt32(2);
            
            us.tipo = dr.GetString(3);
            return us;

        }
        public void insertarTripulante(Tripulacion us)
        {

            try
            {
                ConexionBD();
                /*conexion.Open();*/
                cmd = new NpgsqlCommand("INSERT INTO proyecto1.tripulanciones(cedula, nombre, fecha_nac, id_aerolinea, rol, estado)VALUES ('" + us.cedula + "', '" + us.nombre + "','" + us.fechaNacimiento+ "', '" + us.idAerolinea + "','"+us.rol+"','"+us.estado+ "')", conexion);

                cmd.ExecuteNonQuery();
                conexion.Close();
            }
            catch (Exception E)
            {
                
                    MessageBox.Show("Error");

                
            }
        }
        public List<Tripulacion> ConsultarDatostripulante()
        {


            ConexionBD();
            List<Tripulacion> lista = new List<Tripulacion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT cedula, nombre, fecha_nac, id_aerolinea, rol, estado FROM proyecto1.tripulanciones", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarTripulacion(dr));
                }
            }
            conexion.Close();
            return lista;
        }
        public Tripulacion cargarTripulacion(NpgsqlDataReader dr)
        {
            Tripulacion us = new Tripulacion();
            us.cedula = dr.GetInt32(0);
            us.nombre = dr.GetString(1);
            us.fechaNacimiento = dr.GetDateTime(2);
            us.idAerolinea = dr.GetInt32(3);
            us.rol = dr.GetString(4);
            us.estado = dr.GetString(5);
            return us;

        }

        internal void insertarAvion(Avion a)
        {
            try
            {
                ConexionBD();
                cmd = new NpgsqlCommand("INSERT INTO proyecto1.avion( modelo, ano_construccion, id_aerolinea, capacidad, estado)VALUES('" + a.modelo + "', '" + a.ano_construccion + "', '" + a.id_aerolinea + "', '" + a.capacidad + "', '" + a.estado + "')", conexion);

                cmd.ExecuteNonQuery();
                conexion.Close();
            }
            catch (Exception e)
            {
                if (e.Message.Contains("avio_id_aerolinea_fkey"))
                {
                    MessageBox.Show("Id de aerolinea inexistente.");
                }
            }

        }
        public List<Avion> ConsultarDatosAvion()
        {


            ConexionBD();
            List<Avion> lista = new List<Avion>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, modelo, ano_construccion, id_aerolinea, capacidad, estado FROM proyecto1.avion", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarAviones(dr));
                }
            }
            conexion.Close();
            return lista;
        }
        public Avion cargarAviones(NpgsqlDataReader dr)
        {
            Avion us = new Avion();
            us.id = dr.GetInt32(0);
            us.modelo = dr.GetString(1);
            us.ano_construccion = dr.GetDateTime(2);
            us.id_aerolinea = dr.GetInt32(3);
            us.capacidad = dr.GetInt32(4);
            us.estado = dr.GetString(5);
            return us;

        }



        internal void insertarAeropuerto(Aeropuerto a)
        {
            //try
            //{
                ConexionBD();
                cmd = new NpgsqlCommand("INSERT INTO proyecto1.aeropuerto(  iata, nombre, pais)VALUES('" + a.IATA + "', '" + a.nombre + "', '" + a.pais +"')", conexion);

                cmd.ExecuteNonQuery();
                conexion.Close();
            //}
            //catch (Exception e)
            //{
            //    if (e.Message.Contains("unq_iata"))
            //    {
            //        MessageBox.Show("Aeropuerto ya existente en el sistema.");
            //    }else if (e.Message.Contains("unq_nombre"))
            //    {
            //        MessageBox.Show("Aeropuerto ya existente en el sistema.");
            //    }
            //}

        }

        public List<Aeropuerto> ConsultarDatosAeropuertos()
        {


            ConexionBD();
            List<Aeropuerto> lista = new List<Aeropuerto>();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT id, iata, nombre, pais FROM proyecto1.aeropuerto", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    lista.Add(cargarAeropuertos(dr));
                }
            }
            conexion.Close();
            return lista;
        }
        public Aeropuerto cargarAeropuertos(NpgsqlDataReader dr)
        {
            Aeropuerto us = new Aeropuerto();
            us.id = dr.GetInt32(0);
            us.IATA = dr.GetString(1);
            us.nombre = dr.GetString(2);
            us.pais = dr.GetString(3);
            return us;

        }
    }
}
