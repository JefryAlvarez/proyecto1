﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Entidades
{
    class Aeropuerto
    {
        public int id { get; set; }
        public string IATA { get; set; }
        public string nombre { get; set; }
        public  string pais { get; set; }
    }
}
