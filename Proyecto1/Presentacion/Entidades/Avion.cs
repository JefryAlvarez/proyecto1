﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Entidades
{
    class Avion
    {
        public int id { get; set; }
        public string modelo { get; set; }
        public DateTime ano_construccion { get; set; }

        public int id_aerolinea { get; set; }
        public int capacidad { get; set; }
        public string estado { get; set; }
    }
}
