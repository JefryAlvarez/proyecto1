﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.Entidades
{
    class Tripulacion
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public int idAerolinea { get; set; }
        public string rol { get; set; }
        public string estado { get; set; }
    }
}
