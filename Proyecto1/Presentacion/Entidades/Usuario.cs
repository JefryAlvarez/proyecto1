﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Entidades
{
    class Usuario
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public int edad { get; set; }
        public string contrasena { get; set; }
        public string tipo { get; set; }
        public DateTime fechanacimiento { get; set; }
        public int id_aerolinea { get; set; }
        public string rol { get; set; }
        public string estado { get; set; }
    }
}
