﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Entidades
{
    class Vuelo
    {
		public int id { get; set; }
		public int id_aerolinea { get; set; }
		public int id_aeropuertollegada { get; set; }
		public int id_aeropuertosalida { get; set; }
		public int precio { get; set; }
		public DateTime fecha_salida { get; set; }
		public TimeSpan hora_salida { get; set; }
		public string aeropuertoLlegada { get; set; }
		public  DateTime fecha_llegada { get; set; }

		public TimeSpan hora_llegada { get; set; }
		public string horallegada { get; set; }
		public string aeropuerto_llegada { get; set; }
		public int id_avion { get; set; }
		public string id_tripulacion { get; set; }
    }
	
}
