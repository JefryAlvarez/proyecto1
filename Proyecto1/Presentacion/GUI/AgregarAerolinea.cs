﻿using Presentacion.Entidades;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.GUI
{
    public partial class AgregarAerolinea : Form
    {
        public AgregarAerolinea()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != null)
            {
                if (txtAnno.Text != null)
                {
                    if (txtTipo != null)
                    {
                        try
                        {
                            Aerolinea aero = new Aerolinea();
                            aero.nombre = txtNombre.Text;
                            aero.ano_fundacion = Int32.Parse(txtAnno.Text);
                            aero.tipo = txtTipo.Text;
                            new AerolineaN().insertarAerolinea(aero);
                            txtAnno.Clear();
                            txtNombre.Clear();
                            txtTipo.Clear();
                        }
                        catch (Exception a)
                        {
                            MessageBox.Show("Digite un numero entero.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("No puede dejar el tipo en blanco.");
                    }
                }
                else
                {
                    MessageBox.Show("No puede dejar el año en blanco.");
                }
            }
            else
            {
                MessageBox.Show("No puede dejar el nombre en blanco.");
            }
            

        }
    }
}
