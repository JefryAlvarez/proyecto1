﻿using Presentacion.Entidades;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.GUI
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aeropuerto ar = new Aeropuerto();
            ar.IATA = txtIATA.Text;
            ar.nombre = txtNombre.Text;
            ar.pais = txtPais.Text;
            new AeropuertoN().insertarAeropuerto(ar);
            txtIATA.Clear();
            txtNombre.Clear();
            txtPais.Clear();
        }
    }
}
