﻿using Presentacion.Entidades;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.GUI
{
    public partial class AgregarAvion : Form
    {
        public AgregarAvion()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void label6_Click(object sender, EventArgs e)
        {
                    }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtIdAerolinea.Text != null && txtModelo.Text != null && txtEstado.Text != null && txtCapacidad.Text != null)
            {


                Avion av = new Avion();
                av.modelo = txtModelo.Text;
                av.estado = txtEstado.Text;
                av.ano_construccion = dtpAnoC.Value;
                av.capacidad = Int32.Parse(txtCapacidad.Text);
                av.id_aerolinea = Int32.Parse(txtIdAerolinea.Text);
                new AvionN().insertar(av);
                txtCapacidad.Clear();
                txtEstado.Clear();
                txtIdAerolinea.Clear();
                txtModelo.Clear();
            }
            else
            {
                MessageBox.Show("Debe rellenar todos los espacios");
            }
        }


        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
