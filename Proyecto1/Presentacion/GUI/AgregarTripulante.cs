﻿using Presentacion.Entidades;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.GUI
{
    public partial class AgregarTripulante : Form
    {
        public AgregarTripulante()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Tripulacion trip = new Tripulacion();
                trip.cedula = Int32.Parse(txtCedula.Text);
                trip.estado = txtEstado.Text;
                trip.fechaNacimiento = dtmFechaN.Value.Date;
                trip.nombre = txtNombre.Text;
                trip.idAerolinea = Int32.Parse(txtIdAero.Text);
                trip.rol = txtRol.Text;
                new TripulacionN().agregarTripulante(trip);
                txtCedula.Clear();
                txtEstado.Clear();
                txtIdAero.Clear();
                txtNombre.Clear();
                txtRol.Clear();
                
            }
            catch
            {

            }
        }
    }
}
