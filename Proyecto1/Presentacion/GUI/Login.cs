﻿using Presentacion.Entidades;
using Presentacion.GUI;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
            try {
                Usuario us = new Usuario();
                us.cedula = Int32.Parse(txtCedula.Text);
                us.contrasena = txtContra.Text;
                if(new UsuarioN().inicioSesion(us)==1)
                {
                    MenuAdmin men = new MenuAdmin();
                    Hide();
                    men.ShowDialog();
                    Show();
                }else if (new UsuarioN().inicioSesion(us) == 2)
                {
                    MenuPasajero menup = new MenuPasajero();
                    Hide();
                    menup.ShowDialog();
                    Show();
                }else if(new UsuarioN().inicioSesion(us) == 3)
                {
                    MessageBox.Show("Contraseña incorrecta");
                }else if(new UsuarioN().inicioSesion(us) == 4)
                {
                    MessageBox.Show("Usuario no encontrado");
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Ingrese formato incorrecto, ingrese un numero.");
            }
            
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registrar reg = new Registrar();
            reg.ShowDialog();
            Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            txtCedula.Clear();
            txtContra.Clear();
        }
    }
}
