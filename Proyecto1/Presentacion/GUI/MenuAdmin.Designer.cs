﻿namespace Presentacion.GUI
{
    partial class MenuAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dtgTripulacion = new System.Windows.Forms.DataGridView();
            this.NombreT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvAerolinea = new System.Windows.Forms.DataGridView();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Año = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.FechaN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdAerolinea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvAvion = new System.Windows.Forms.DataGridView();
            this.IdAvion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AnoConst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdAero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Capacidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoAvion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvAeropuertos = new System.Windows.Forms.DataGridView();
            this.IdAeroP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.IdVuelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdAerolinea2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaSalida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraSalida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AeropuertoS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaLL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoraLlegada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AeropuertoLL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Idavion2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdTripulantes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgTripulacion)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).BeginInit();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuertos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.dataGridView5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(422, 327);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Vuelos";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click_1);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button2);
            this.tabPage5.Controls.Add(this.dgvAeropuertos);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(422, 327);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Aeropuertos";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnAgregar);
            this.tabPage4.Controls.Add(this.dgvAvion);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(422, 327);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Aviones";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.dtgTripulacion);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(422, 327);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tripulaciones";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // dtgTripulacion
            // 
            this.dtgTripulacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgTripulacion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cedula,
            this.NombreT,
            this.FechaN,
            this.IdAerolinea,
            this.Rol,
            this.Estado});
            this.dtgTripulacion.Location = new System.Drawing.Point(2, 0);
            this.dtgTripulacion.Name = "dtgTripulacion";
            this.dtgTripulacion.Size = new System.Drawing.Size(420, 283);
            this.dtgTripulacion.TabIndex = 0;
            this.dtgTripulacion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // NombreT
            // 
            this.NombreT.HeaderText = "Nombre";
            this.NombreT.Name = "NombreT";
            // 
            // Cedula
            // 
            this.Cedula.HeaderText = "Cedula";
            this.Cedula.Name = "Cedula";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.dgvAerolinea);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(422, 327);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Aerolineas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvAerolinea
            // 
            this.dgvAerolinea.AllowUserToDeleteRows = false;
            this.dgvAerolinea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAerolinea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Nombre,
            this.Año,
            this.Tipo});
            this.dgvAerolinea.Location = new System.Drawing.Point(0, 0);
            this.dgvAerolinea.Name = "dgvAerolinea";
            this.dgvAerolinea.ReadOnly = true;
            this.dgvAerolinea.Size = new System.Drawing.Size(422, 285);
            this.dgvAerolinea.TabIndex = 0;
            this.dgvAerolinea.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Tipo
            // 
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            // 
            // Año
            // 
            this.Año.HeaderText = "Año";
            this.Año.Name = "Año";
            this.Año.ReadOnly = true;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(10, 34);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(430, 353);
            this.tabControl1.TabIndex = 0;
            // 
            // FechaN
            // 
            this.FechaN.HeaderText = "Fecha Nacimiento";
            this.FechaN.Name = "FechaN";
            // 
            // IdAerolinea
            // 
            this.IdAerolinea.HeaderText = "Id-Aerolinea";
            this.IdAerolinea.Name = "IdAerolinea";
            // 
            // Rol
            // 
            this.Rol.HeaderText = "Rol";
            this.Rol.Name = "Rol";
            // 
            // Estado
            // 
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            // 
            // dgvAvion
            // 
            this.dgvAvion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAvion.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdAvion,
            this.Modelo,
            this.AnoConst,
            this.IdAero,
            this.Capacidad,
            this.EstadoAvion});
            this.dgvAvion.Location = new System.Drawing.Point(2, 1);
            this.dgvAvion.Name = "dgvAvion";
            this.dgvAvion.Size = new System.Drawing.Size(417, 279);
            this.dgvAvion.TabIndex = 0;
            this.dgvAvion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellContentClick);
            // 
            // IdAvion
            // 
            this.IdAvion.HeaderText = "Id";
            this.IdAvion.Name = "IdAvion";
            // 
            // Modelo
            // 
            this.Modelo.HeaderText = "Modelo";
            this.Modelo.Name = "Modelo";
            // 
            // AnoConst
            // 
            this.AnoConst.HeaderText = "Año Construcción";
            this.AnoConst.Name = "AnoConst";
            // 
            // IdAero
            // 
            this.IdAero.HeaderText = "Id Aerolinea";
            this.IdAero.Name = "IdAero";
            // 
            // Capacidad
            // 
            this.Capacidad.HeaderText = "Capacidad";
            this.Capacidad.Name = "Capacidad";
            // 
            // EstadoAvion
            // 
            this.EstadoAvion.HeaderText = "Estado";
            this.EstadoAvion.Name = "EstadoAvion";
            // 
            // dgvAeropuertos
            // 
            this.dgvAeropuertos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAeropuertos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdAeroP,
            this.IATA,
            this.NombreA,
            this.Pais});
            this.dgvAeropuertos.Location = new System.Drawing.Point(2, 1);
            this.dgvAeropuertos.Name = "dgvAeropuertos";
            this.dgvAeropuertos.Size = new System.Drawing.Size(420, 286);
            this.dgvAeropuertos.TabIndex = 0;
            this.dgvAeropuertos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick);
            // 
            // IdAeroP
            // 
            this.IdAeroP.HeaderText = "Id";
            this.IdAeroP.Name = "IdAeroP";
            // 
            // IATA
            // 
            this.IATA.HeaderText = "IATA";
            this.IATA.Name = "IATA";
            // 
            // NombreA
            // 
            this.NombreA.HeaderText = "Nombre";
            this.NombreA.Name = "NombreA";
            // 
            // Pais
            // 
            this.Pais.HeaderText = "País";
            this.Pais.Name = "Pais";
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdVuelo,
            this.IdAerolinea2,
            this.Precio,
            this.FechaSalida,
            this.HoraSalida,
            this.AeropuertoS,
            this.FechaLL,
            this.HoraLlegada,
            this.AeropuertoLL,
            this.Idavion2,
            this.IdTripulantes});
            this.dataGridView5.Location = new System.Drawing.Point(1, 1);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(420, 281);
            this.dataGridView5.TabIndex = 0;
            this.dataGridView5.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView5_CellContentClick);
            // 
            // IdVuelo
            // 
            this.IdVuelo.HeaderText = "Id";
            this.IdVuelo.Name = "IdVuelo";
            // 
            // IdAerolinea2
            // 
            this.IdAerolinea2.HeaderText = "Id Aerolinea";
            this.IdAerolinea2.Name = "IdAerolinea2";
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            // 
            // FechaSalida
            // 
            this.FechaSalida.HeaderText = "Fecha de salida";
            this.FechaSalida.Name = "FechaSalida";
            // 
            // HoraSalida
            // 
            this.HoraSalida.HeaderText = "Hora de salida";
            this.HoraSalida.Name = "HoraSalida";
            // 
            // AeropuertoS
            // 
            this.AeropuertoS.HeaderText = "Aeropuerto de salida";
            this.AeropuertoS.Name = "AeropuertoS";
            // 
            // FechaLL
            // 
            this.FechaLL.HeaderText = "Fecha llegada";
            this.FechaLL.Name = "FechaLL";
            // 
            // HoraLlegada
            // 
            this.HoraLlegada.HeaderText = "Hora de llegada";
            this.HoraLlegada.Name = "HoraLlegada";
            // 
            // AeropuertoLL
            // 
            this.AeropuertoLL.HeaderText = "Aeropuerto de llegada";
            this.AeropuertoLL.Name = "AeropuertoLL";
            // 
            // Idavion2
            // 
            this.Idavion2.HeaderText = "Id Avion";
            this.Idavion2.Name = "Idavion2";
            // 
            // IdTripulantes
            // 
            this.IdTripulantes.HeaderText = "Id tripulantes";
            this.IdTripulantes.Name = "IdTripulantes";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(2, 279);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(420, 45);
            this.btnAgregar.TabIndex = 1;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(2, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(420, 45);
            this.button1.TabIndex = 2;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(0, 286);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(420, 45);
            this.button2.TabIndex = 3;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(0, 282);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(420, 45);
            this.button3.TabIndex = 3;
            this.button3.Text = "Agregar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(0, 282);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(420, 45);
            this.button4.TabIndex = 4;
            this.button4.Text = "Agregar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(352, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cerrar sesión";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // MenuAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(452, 390);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Name = "MenuAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuAdm";
            this.Activated += new System.EventHandler(this.MenuAdmin_Activated);
            this.Load += new System.EventHandler(this.MenuAdmin_Load);
            this.tabPage1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgTripulacion)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAerolinea)).EndInit();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAeropuertos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgvAvion;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdAvion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnoConst;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdAero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Capacidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoAvion;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dtgTripulacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreT;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaN;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdAerolinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvAerolinea;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Año;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.DataGridView dgvAeropuertos;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdAeroP;
        private System.Windows.Forms.DataGridViewTextBoxColumn IATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pais;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdVuelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdAerolinea2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaSalida;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraSalida;
        private System.Windows.Forms.DataGridViewTextBoxColumn AeropuertoS;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaLL;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoraLlegada;
        private System.Windows.Forms.DataGridViewTextBoxColumn AeropuertoLL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Idavion2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdTripulantes;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
    }
}