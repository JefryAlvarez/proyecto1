﻿using Presentacion.Entidades;
using Presentacion.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion.GUI
{
    public partial class MenuAdmin : Form
    {
        public MenuAdmin()
        {
            InitializeComponent();
        }
        public void cargarAero()
        {
            AerolineaN aero = new AerolineaN();

            dgvAerolinea.Rows.Clear();
            if (aero.cargarAerolinea()!=null)
            {
                foreach(Aerolinea a in aero.cargarAerolinea())
                {
                    dgvAerolinea.Rows.Add(a.id,a.nombre,a.ano_fundacion,a.tipo);
                }
            }
        } 
        public void cargarTripulante()
        {
            TripulacionN trip = new TripulacionN();
            dtgTripulacion.Rows.Clear();
            if (trip.cargartripulantes() != null)
            {
                foreach(Tripulacion t in trip.cargartripulantes())
                {
                    dtgTripulacion.Rows.Add(t.cedula,t.nombre,t.idAerolinea,t.fechaNacimiento,t.rol,t.estado);
                }
            }
        }
        public void cargarAvion()
        {
            AvionN av = new AvionN();
            dgvAvion.Rows.Clear();
            if (av.cargarAvion() != null)
            {
                foreach(Avion a in av.cargarAvion())
                {
                    dgvAvion.Rows.Add(a.id,a.modelo,a.ano_construccion,a.id_aerolinea,a.capacidad,a.estado);
                }
            }
        }
        public void cargarAeropuerto()
        {
            AeropuertoN ar = new AeropuertoN();
            dgvAeropuertos.Rows.Clear();
            if (ar.cargarAeropuertos() != null)
            {
                foreach(Aeropuerto a in ar.cargarAeropuertos())
                {
                    dgvAeropuertos.Rows.Add(a.id,a.IATA,a.nombre,a.pais);

                }
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView5_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage1_Click_1(object sender, EventArgs e)
        {

        }

        private void MenuAdmin_Load(object sender, EventArgs e)
        {

        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AgregarAvion add3 = new AgregarAvion();
            Hide();
            add3.ShowDialog();
            Show();
        }
        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            AgregarTripulante add2 = new AgregarTripulante();
            Hide();
            add2.ShowDialog();
            Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AgregarAerolinea add = new AgregarAerolinea();
            Hide();
            add.ShowDialog();
            Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MenuAdmin_Activated(object sender, EventArgs e)
        {
            cargarAero();
            cargarTripulante();
            cargarAvion();
            cargarAeropuerto();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 agregar = new Form2();
            Hide();
            agregar.ShowDialog();
            Show();
        }
    }
}
