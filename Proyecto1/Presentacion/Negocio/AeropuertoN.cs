﻿using Presentacion.Entidades;
using Proyecto1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Negocio
{
    class AeropuertoN
    {
        Conexion con = new Conexion();
        public void insertarAeropuerto(Aeropuerto a)
        {
            con.insertarAeropuerto(a);
        }
        public List<Aeropuerto> cargarAeropuertos()
        {
            return con.ConsultarDatosAeropuertos();
        }
    }
}
