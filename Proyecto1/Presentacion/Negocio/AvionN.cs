﻿using Presentacion.Entidades;
using Proyecto1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion.Negocio
{
    class AvionN
    {
        Conexion con = new Conexion();
        public void insertar(Avion av)
        {
            con.insertarAvion(av);
        }
        public List<Avion> cargarAvion()
        {
            return con.ConsultarDatosAvion();
        }
    }
}
